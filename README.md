# Raspberry Pi Python Project Similar with NAVIO+ #

## Hooking up all components.

### Connect GPS module ###
    
http://www.newegg.com/Product/Product.aspx?Item=9SIA7BF3JV1625&nm_mc=KNC-GoogleMKP-PC&cm_mmc=KNC-GoogleMKP-PC-

Pin Connection:

| **GPS**  | **RPi**   |
| ----     |:-------:  |
| GND      | GND       |
| VIN      | 3.3V      |
| TxD      | GPIO 15   |
| RxD      | GPIO 14   |

### Connect Brushless Motor ###

#### Hooking up guide of Brushless Motor and ESC. ####

http://www.ebay.com/itm/like/121561698495?lpid=82&chn=ps&ul_noapp=true

- Read the [User Manual](http://propeleris.lt/failai/wp-s10c-rtr_manual.pdf) carefully before hooking up.

    I set the "Running Mode" of the ESC as "Forward with brake".

- Connect ESC with PCA9685 board.

    There are 3 control cables in ESC. (Red, Black, White.)

    Don't connect red wire. This is for power and we do not need this in our case.

    In my case the white wire is for PWM signal and black wire is for GND.

    | **ESC**      | **PCA9685 (Channel 0)** |
    |:---:         | :----: |
    | PWM (White)  | PWM    |
    | GND (Black)  | GND    |

### Connect PCA96835 with Raspberry Pi.

| **PCA9685**  | **RPi**   |
| :----:       |:---------:|
| GND          | GND       |
| VCC          | 5V        |
| SDA          | SDA       |
| SCL          | SCL       |

*NOTE*:
 Please *DO NOT* confuse **VCC** with **V+**!

 In some tutorials of PCA9685, they are saying that VCC should be connected to 3.3V.

 But in this case the amplitude of RPi's PWM output is 3.3 volt that ESC does not recognize!
 (I had to buy oscilloscope to fix this issue...)

### Connect Servo motor with PCA9685

| **PCA9685**  | **Servo**   |
| :----:       |:---------:|
| GND          | GND       |
| V+          |  VCC       |
| PWM(of channel 1) | SDA (PWM)       |

In my case servo motor has 3 wires - Red(VCC), Orange(PWM), Brown(GND)

Connect power supply(relative to the servo's operating voltage) through the blue terminal block.

## Test all components. ##

### Prepairing Raspberry Pi.
 
- Expand file system in **raspi-config** by following below.

    https://www.raspberrypi.org/documentation/configuration/raspi-config.md

- Update and upgrade packages

        sudo apt-get update
        sudo apt-get upgrade

- Configure I2C by following bellow.
    
    Since we are using Adafruit PCA9685 PWM board, we need to enable I2C for Raspberry Pi. 
    
    https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c
    
    The I2C address of PCA9685 is 0x40 & 0x70.
    
    So if you have no problem, you should see these values with `sudo i2cdetect -y 1` command.
    
    ![I2C Test](img/i2c_test.jpg "I2C Test")

- Enable Serial Port for GPS module.

    By default the Raspberry Pi’s serial port is configured to be used for console input/output. 
    Whilst this is useful if you want to login using the serial port, it means you can't use the Serial Port in your programs. 
    To be able to use the serial port to connect and talk to GPS module, the serial port console login needs to be disabled.
    
    Open the `/boot/cmdline.txt` and disable serial.
    
        sudo nano /boot/cmdline.txt
        
    The contents of the file look like this

        dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait

    Remove all references to *ttyAMA0* (which is the name of the serial port). 
    The file will now look like this

        dwc_otg.lpm_enable=0 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait

### Clone source code

    cd ~    
    https://username@bitbucket.org/rpi_guru/navio_ctrl.git
    
`username` is the Bitbucket user account name.

    
### Test GPS module ###

- Install dependencies for GPS data.
    
        sudo apt-get install python-pip
        sudo pip install pyserial
        
- Acquire GPS data from the module.
    
    Use the command

        stty -F /dev/ttyAMA0

    to see the current settings.

    and

        stty -F /dev/ttyAMA0 9600

    to set a baud rate of 9600 (4800 is more normal for GPS).

    Try

        cat </dev/ttyAMA0

    to see if your GPS unit is properly connected.

    ![GPS data](img/gps_test.jpg "GPS Test")
    
- Get velocity from the GPS data.
    
        cd ~/navio_ctrl/test
        python gps.py
        
    ![Velocity data](img/test_gps_2.jpg "Velocity")
    
### Test Brushless motor ###
  
- Install dependencies for the ESC of Brushless motor.
    
        sudo apt-get install git build-essential python-dev
        cd ~
        git clone https://github.com/adafruit/Adafruit_Python_PCA9685.git
        cd Adafruit_Python_PCA9685
        sudo python setup.py install

- Test driving the motor.
    
    Turn ESC off and run this:
    
        sudo python ~/navio_ctrl/test/adafruit_pwm.py
    
    Input PWM width as 500~1000 to give maximum throttle to ESC.
    
    Turn ESC on and watch the red LED.
    
    At this time ESC's LED should be turned off if it detects PWM input signal.
    
    Otherwise if ESC's LED is still blinking there are some problems and we need to fix it.
        
    Input PWM over 300.
    
    In my case motor started moving from 325.
    
    (In the code, I set PWM frequency as 45.5Hz, not 50Hz.
    Thought I set it as 50Hz, actually my oscilloscope says that its frequency is 55Hz.
    Very odd why PWM output frequency is differ.)
    
    ![Diagram](img/diagram.jpg "Diagram")
        
### Test servo motor.

I have tested with my SG90 servo motor.

It's pulse width is 500-2400us.

So I set PWM's frequency as 40Hz.

In this case, servo moved to the 0 degree position when I gave pwm width value to 100.

And if I gave 500, it moved to 180 degree position.

Actually we need to tweak parameters since those are vary for each servo types.



  


        

